#pragma once
#include "Scene.h"
#include "GameObject.h"
#include "Player.h"
#include "Enemy.h"
#include <vector>
#include "text.h"
#include "PowerUp.h"
#include "BossEnemy.h"
#include "Bullet.h"

class GameScene : public Scene
{
public:
	GameScene();
	~GameScene();
	void start();
	void draw();
	void update();

private:
	Player* player;
	PowerUp* powerUp;

	//enemy spawn logic
	float spawnTime;
	float currentSpwanTimer;
	std::vector<Enemy*> spwanedEnemies;

	//power up spawn timer
	float timer;
	float currentTimer;

	void spawnLogic();
	void collisionLogic();

	//boss logic
	void spawnBoss();
	std::vector<BossEnemy*> spawnedBoss;
	bool bossIsAlive;

	void spawnPowerUP();
	void pickUpPowerUp();

	void spawn();
	void despawnEnemy(Enemy* enemy);
	void despawnAllEnemies();

	int points;
	int powerUpCounter;
};

