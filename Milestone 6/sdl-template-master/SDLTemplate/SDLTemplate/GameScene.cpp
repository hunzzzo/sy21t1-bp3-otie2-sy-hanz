#include "GameScene.h"

GameScene::GameScene()
{
	// Register and add game objects on constructor
	player = new Player();
	this->addGameObject(player);
	//do ^^this for any game object in the scene

	points = 0;
}

GameScene::~GameScene()
{
	delete player;
}

inline void GameScene::start()
{
	Scene::start();
	// Initialize any scene logic here

	initFonts();

	// enemy spawn time
	currentSpwanTimer = 210;
	spawnTime = 210; //spawn time of 3.5 seconds

	// power up spawn time
	timer = 1200;
	currentTimer = 1200; // spawn time 20 seconds

	powerUpCounter = 0;

	for (int i = 0; i < 3; i++)
	{
		spawn();
	}
}

void GameScene::draw()
{
	Scene::draw();
	
	drawText(110, 20, 255, 255, 255, TEXT_CENTER, "POINTS: %03d", points);

	if (player->getIsAlive() == false)
	{
		drawText(SCREEN_WIDTH / 2, 600, 255, 255, 255, TEXT_CENTER, "GAME OVER");
	}
}

void GameScene::update()
{
	Scene::update();

	//spawn functions
	spawnPowerUP();
	if (!bossIsAlive)
	{
		spawnLogic();
	}

	//collision functions
	if (player->getIsAlive())
	{
		collisionLogic();
		pickUpPowerUp();
	}

	//boss battle
	if (points > 0)
	{
		if (points % 20 == 0)
		{
			despawnAllEnemies();

			if (!bossIsAlive)
			{
				spawnBoss();
				bossIsAlive = true;
			}
		}
	}
}

void GameScene::spawnLogic()
{
	if (currentSpwanTimer > 0)
		currentSpwanTimer--;

	if (currentSpwanTimer <= 0)
	{
		for (int i = 0; i < rand() % 3 + 3; i++)
		{
			spawn();
		}
		currentSpwanTimer = spawnTime;
	}
}

void GameScene::collisionLogic()
{
	for (int i = 0; i < objects.size(); i++)
	{
		// cast to bullet 
		Bullet* bullet = dynamic_cast<Bullet*>(objects[i]);

		// checks if objects[i] was a bullet
		if (bullet != NULL)
		{
			// if the bullet was from the enemy
			if (bullet->getSide() == Side::ENEMY_SIDE)
			{
				int collisison = checkCollision(
					player->getPosX(), player->getPosY(), player->getWidth(), player->getHeight(),
					bullet->getPosX(), bullet->getPosY(), bullet->getBulletWidth(), bullet->getBulletHeight()
				);

				if (collisison == 1)
				{
					std::cout << "player was hit!" << std::endl;
					player->dedEfx();
					break;
				}
			}
			else if (bullet->getSide() == Side::PLAYER_SIDE) // if bullet from player
			{
				for (int i = 0; i < spwanedEnemies.size(); i++)
				{
					Enemy* currentEnemy = spwanedEnemies[i];

					int collisison = checkCollision(currentEnemy->getPosX(), currentEnemy->getPosY(), 
													currentEnemy->getWidth(), currentEnemy->getHeight(),
													bullet->getPosX(), bullet->getPosY(), 
													bullet->getBulletWidth(), bullet->getBulletHeight());

					if (collisison == 1)
					{
						std::cout << "enemy was hit!" << std::endl;
						player->eraseBullet(bullet);
						despawnEnemy(currentEnemy);
						points++;
						break;
					}
				}

				for (int i = 0; i < spawnedBoss.size(); i++)
				{
					BossEnemy* boss = spawnedBoss[i];
					int collisison = checkCollision(boss->getPosX(), boss->getPosY(),
						boss->getWidth(), boss->getHeight(),
						bullet->getPosX(), bullet->getPosY(),
						bullet->getBulletWidth(), bullet->getBulletHeight());

					if (collisison == 1)
					{
						std::cout << "boss was hit!" << std::endl;
						boss->bossTakeDamage();

						player->eraseBullet(bullet);

						if (boss->getBossHp() <= 0)
						{
							delete boss;
							bossIsAlive = false;
							points++;
						}
						break;
					}
				}
			}
		}
	}
}

void GameScene::spawnBoss()
{
	BossEnemy* boss = new BossEnemy();
	this->addGameObject(boss);
	spawnedBoss.push_back(boss);
}

void GameScene::spawnPowerUP()
{
	if (currentTimer > 0)
		currentTimer--;

	if (currentTimer <= 0)
	{
		PowerUp* powerUp = new PowerUp();
		this->addGameObject(powerUp);

		currentTimer = timer;
	}
}

void GameScene::pickUpPowerUp()
{
	for (int i = 0; i < objects.size(); i++)
	{
		PowerUp* powerUp = dynamic_cast<PowerUp*>(objects[i]);

		if (powerUp != NULL)
		{
			int collisison = checkCollision(player->getPosX(), player->getPosY(), 
											player->getWidth(), player->getHeight(),
											powerUp->getPosX(), powerUp->getPosY(), 
											powerUp->getPowerUpWidth(), powerUp->getPowerUpHeight());

			if (collisison == 1)
			{
				std::cout << "powerup taken and deleted" << std::endl;
				player->playerPowerUp();
				powerUpCounter++;
				delete powerUp;
				break;
			}
		}
	}
}

void GameScene::spawn()
{
	Enemy* enemy = new Enemy();
	this->addGameObject(enemy);
	enemy->setPlayerTarget(player);

	enemy->setPosition(rand() % SCREEN_WIDTH, -10);
	spwanedEnemies.push_back(enemy);
}

void GameScene::despawnEnemy(Enemy* enemy)
{
	int index = -1;
	for (int i = 0; i < spwanedEnemies.size(); i++)
	{
		if (enemy == spwanedEnemies[i])
		{
			index = i;
			break;
		}
	}

	if (index != -1)
	{
		spwanedEnemies.erase(spwanedEnemies.begin() + index);
		delete enemy;
	}
}

void GameScene::despawnAllEnemies()
{
	for (int i = 0; i < spwanedEnemies.size(); i++)
	{
		Enemy* currentEnemy = spwanedEnemies[i];
		despawnEnemy(currentEnemy);
	}
}
