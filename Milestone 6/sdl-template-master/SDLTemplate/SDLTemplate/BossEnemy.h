#pragma once
#include "GameObject.h"
#include "common.h" 
#include "draw.h"
#include "SoundManager.h"
#include "Bullet.h"
#include <vector>

class BossEnemy :
    public GameObject
{
public:
    BossEnemy();
    ~BossEnemy();
    void start();
    void update();
    void draw();

    int getBossHp();
    void setBossHp(int hp);

    int getPosX();
    int getPosY();
    void checkEdges(int positionX);
    void bossMovement();
    void bossTakeDamage();

    int getWidth();
    int getHeight();

    void firingBullets();

    void memoryManageBullets();

private:
    SDL_Texture* texture;
    Mix_Chunk* sound;
    int bossHp;
    int posX;
    int posY;
    float directionX;
    float directionY;
    int bossWidth;
    int bossHeight;
    int bossSpeed;
    float reloadTime;
    float currentReloadTime;
    float directionChangeTime;
    float currentDirectionChangeTime;
    std::vector<Bullet*> bullets;
};

