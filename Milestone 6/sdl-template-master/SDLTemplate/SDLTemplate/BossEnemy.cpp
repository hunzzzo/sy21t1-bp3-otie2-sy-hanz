#include "BossEnemy.h"
#include "Scene.h"

BossEnemy::BossEnemy()
{
}

BossEnemy::~BossEnemy()
{
}

void BossEnemy::start()
{
	texture = loadTexture("gfx/BossEnemy.png");

	bossHp = 100;
	posX = rand() % SCREEN_WIDTH;
	posY = 20;
	directionX = 1;
	directionY = 0;
	bossWidth = 0;
	bossHeight = 0;
	bossSpeed = 2;

	reloadTime = rand() % 30 + 60;
	currentReloadTime = 0;

	directionChangeTime = rand() % 30 + 60;
	currentDirectionChangeTime = 0;

	SDL_QueryTexture(texture, NULL, NULL, &bossWidth, &bossHeight);

	sound = SoundManager::loadSound("sound/334227__jradcoolness__laser.ogg");
	sound->volume = 40;
}

void BossEnemy::update()
{
	bossMovement();
	checkEdges(posX);

	firingBullets();
	memoryManageBullets();
}

void BossEnemy::draw()
{
	blit(texture, posX, posY);
}

int BossEnemy::getBossHp()
{
	return bossHp;
}

void BossEnemy::setBossHp(int hp)
{
	bossHp = hp;
}

int BossEnemy::getPosX()
{
	return posX;
}

int BossEnemy::getPosY()
{
	return posY;
}

void BossEnemy::checkEdges(int positionX)
{
	if (positionX <= 0)
	{
		directionX = -directionX;
	}
	else if (positionX >= SCREEN_WIDTH)
	{
		directionX = -directionX;
	}
}

void BossEnemy::bossMovement()
{
	directionChangeTime = rand() % 30 + 60;

	posX += directionX * bossSpeed;

	if (currentDirectionChangeTime > 0)
		currentDirectionChangeTime--;

	if (currentDirectionChangeTime == 0)
	{
		directionX = rand() % 3;

		if (directionX == 2)
		{
			directionX = -1;
		}

		currentDirectionChangeTime = directionChangeTime;
	}

	posX += directionX * bossSpeed;
}

void BossEnemy::bossTakeDamage()
{
	bossHp--;
}

int BossEnemy::getWidth()
{
	return bossWidth;
}

int BossEnemy::getHeight()
{
	return bossHeight;
}

void BossEnemy::firingBullets()
{
	reloadTime = rand() % 30 + 60;

	if (currentReloadTime > 0)
		currentReloadTime--;

	if (currentReloadTime == 0)
	{
		float dx = 0;
		float dy = 1;

		SoundManager::playSound(sound);
		for (int i = 0; i < rand() % 7 + 3; i++)
		{
			Bullet* bullet = new Bullet(posX + rand() % 150 + 10, posY + 30, 
										dx, dy, 10, Side::ENEMY_SIDE);
			bullets.push_back(bullet);
			getScene()->addGameObject(bullet);
		}
		
		//starts realod time 
		currentReloadTime = reloadTime;
	}
}

void BossEnemy::memoryManageBullets()
{
	for (int i = 0; i < bullets.size(); i++)
	{
		if (bullets[i]->getPosX() >= SCREEN_WIDTH)
		{
			Bullet* bulletToErase = bullets[i];
			bullets.erase(bullets.begin() + i);
			delete bulletToErase;
			std::cout << "boss bullet erased" << std::endl;
			break;
		}

		if (bullets[i]->getPosX() <= 0)
		{
			Bullet* bulletToErase = bullets[i];
			bullets.erase(bullets.begin() + i);
			delete bulletToErase;
			std::cout << "boss bullet erased" << std::endl;
			break;
		}

		if (bullets[i]->getPosY() >= SCREEN_HEIGHT)
		{
			Bullet* bulletToErase = bullets[i];
			bullets.erase(bullets.begin() + i);
			delete bulletToErase;
			std::cout << "boss bullet erased" << std::endl;
			break;
		}
	}
}
