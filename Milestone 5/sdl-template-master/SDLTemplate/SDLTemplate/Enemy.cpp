#include "Enemy.h"
#include "Scene.h"

Enemy::Enemy()
{
}

Enemy::~Enemy()
{
}

void Enemy::start()
{
	texture = loadTexture("gfx/enemy.png");

	directionX = 0;
	directionY = 1;
	enemyWidth = 0;
	enemyHeight = 0;
	enemySpeed = 1.5;

	reloadTime = 90; //reload time of 90 frames, 1.5 seconds 
	currentReloadTime = 0;

	directionChangeTime = 90;
	currentDirectionChangeTime = 0;

	SDL_QueryTexture(texture, NULL, NULL, &enemyWidth, &enemyHeight);

	sound = SoundManager::loadSound("sound/334227__jradcoolness__laser.ogg");
	sound->volume = 50;
}

void Enemy::update()
{
	posY += directionY * enemySpeed;

	if (currentDirectionChangeTime > 0)
		currentDirectionChangeTime--;

	if (currentDirectionChangeTime == 0)
	{
		directionX = rand() % 3;

		if (directionX == 2)
		{
			directionX = -1;
		}

		currentDirectionChangeTime = directionChangeTime;
	}
	
	posX += directionX * enemySpeed;
	checkEdges(posX);

	if (currentReloadTime > 0)
		currentReloadTime--;

	// firing bullets
	if (currentReloadTime == 0)
	{
		float dx = 0;
		float dy = 1;

		// bullets target player code
		//calcSlope(playerTarget->getPosX(), playerTarget->getPosY(), posX, posY, &dx, &dy);

		SoundManager::playSound(sound);
		Bullet* bullet = new Bullet(posX + 19, posY + 30, dx, dy, 10, Side::ENEMY_SIDE);
		bullets.push_back(bullet);
		getScene()->addGameObject(bullet);

		//starts realod time 
		currentReloadTime = reloadTime;
	}

	// memory manage bullets
	for (int i = 0; i < bullets.size(); i++)
	{
		if (bullets[i]->getPosY() > SCREEN_HEIGHT)
		{	
			Bullet* bulletToErase = bullets[i];
			bullets.erase(bullets.begin() + i);
			delete bulletToErase;
			std::cout << "enemy bullet erased" << std::endl;
			break;
		}
	}
}

void Enemy::draw()
{
	blit(texture, posX, posY);
}

void Enemy::setPlayerTarget(Player* player)
{
	playerTarget = player;
}

void Enemy::setPosition(int x, int y)
{
	this->posX = x;
	this->posY = y;
}

void Enemy::checkEdges(int positionX)
{
	if (positionX <= 0)
	{
		directionX = -directionX;
	}
	else if (positionX >= SCREEN_WIDTH)
	{
		directionX = -directionX;
	}
}

int Enemy::getPosX()
{
	return posX;
}

int Enemy::getPosY()
{
	return posY;
}

int Enemy::getWidth()
{
	return enemyWidth;
}

int Enemy::getHeight()
{
	return enemyHeight;
}
