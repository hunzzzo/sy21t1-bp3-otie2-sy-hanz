#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"

class PowerUp :
    public GameObject
{
public:
    PowerUp();
    ~PowerUp();

    void start();
    void update();
    void draw();

    int getPosX();
    int getPosY();

    int getPowerUpWidth();
    int getPowerUpHeight();

private:
    SDL_Texture* texture;
    int posX;
    int posY;
    float directionX;
    float directionY;
    int powerUpWidth;
    int powerUpHeight;
    //int powerUpCounter;
};

