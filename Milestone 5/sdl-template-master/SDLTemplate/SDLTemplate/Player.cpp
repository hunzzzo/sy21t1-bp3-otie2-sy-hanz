#include "Player.h"
#include "Scene.h"

Player::~Player()
{
	for (int i = 0; i < bullets.size(); i++)
	{
		delete bullets[i];
	}
	bullets.clear();
}

void Player::start()
{
	//load the texture, call loadTexture function(found at draw.cpp), input directory to the file
	//SDL currently only supports jpeg, png, bitmap format
	texture = loadTexture("gfx/PlayerShip.png");

	//initialize values
	posX = 300;
	posY = 600;
	playerWidth = 0;
	playerHeight = 0;
	playerSpeed = 5;

	reloadTime = 10;
	currentReloadTime = 0;

	powerUpCounter = 0;

	isAlive = true;

	//query texture to set width and height
	SDL_QueryTexture(texture, NULL, NULL, &playerWidth, &playerHeight);

	sound = SoundManager::loadSound("sound/334227__jradcoolness__laser.ogg");
}

void Player::update()
{
	for (int i = 0; i < bullets.size(); i++)
	{
		if (bullets[i]->getPosY() < 0)
		{
			Bullet* bulletToErase = bullets[i];
			bullets.erase(bullets.begin() + i);
			delete bulletToErase;
			std::cout << "player bullet erased" << std::endl;
			break;
		}
	}

	if (!isAlive) return;
	//player controls are done under update

	//if "W" is pressed on the keyboard
	if (app.keyboard[SDL_SCANCODE_W]) //move up
	{
		posY -= playerSpeed;
	}

	if (app.keyboard[SDL_SCANCODE_S]) //move down
	{
		posY += playerSpeed;
	}

	if (app.keyboard[SDL_SCANCODE_A]) //move left
	{
		posX -= playerSpeed;
	}

	if (app.keyboard[SDL_SCANCODE_D]) //move right
	{
		posX += playerSpeed;
	}

	if (currentReloadTime > 0)
		currentReloadTime--;

	if (app.keyboard[SDL_SCANCODE_SPACE] && currentReloadTime == 0)
	{
		SoundManager::playSound(sound);
		Bullet* bullet = new Bullet(posX + 19, posY - 10, 1, 0, 10, Side::PLAYER_SIDE);
		bullets.push_back(bullet);
		getScene()->addGameObject(bullet);

		if (powerUpCounter >= 2)
		{
			Bullet* bullet2 = new Bullet(posX, posY, 1, 0, 10, Side::PLAYER_SIDE);
			bullets.push_back(bullet2);
			getScene()->addGameObject(bullet2);

			Bullet* bullet3 = new Bullet(posX + 39, posY, 1, 0, 10, Side::PLAYER_SIDE);
			bullets.push_back(bullet3);
			getScene()->addGameObject(bullet3);
		}
		else if (powerUpCounter >= 1)
		{
			Bullet* bullet2 = new Bullet(posX, posY, 1, 0, 10, Side::PLAYER_SIDE);
			bullets.push_back(bullet2);
			getScene()->addGameObject(bullet2);
		}

		//starts realod time 
		currentReloadTime = reloadTime;
	}
}

void Player::draw()
{
	if (!isAlive) return;

	//call blit function to draw the texture
	//pass in texture, position X and Y
	blit(texture, posX, posY);

	//go to GameScene.h and #inlcude "player.h" there 
	//then create an instance of the player or any object 
}

int Player::getPosX()
{
	return posX;
}

int Player::getPosY()
{
	return posY;
}

int Player::getWidth()
{
	return playerWidth;
}

int Player::getHeight()
{
	return playerHeight;
}

bool Player::getIsAlive()
{
	return isAlive;
}

void Player::dedEfx()
{
	isAlive = false;
}

void Player::playerPowerUp()
{
	powerUpCounter++;
}
