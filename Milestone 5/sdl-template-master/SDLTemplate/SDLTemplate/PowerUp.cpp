#include "PowerUp.h"

PowerUp::PowerUp()
{
}

PowerUp::~PowerUp()
{
	
}

void PowerUp::start()
{
	posX = rand() % SCREEN_WIDTH;
	posY = -10;
	powerUpWidth = 0;
	powerUpHeight = 0;

	directionX = 0;
	directionY = 1.5;

	texture = loadTexture("gfx/points.png");

	SDL_QueryTexture(texture, NULL, NULL, &powerUpWidth, &powerUpHeight);
}

void PowerUp::update()
{
	posY += directionY * 2;
}

void PowerUp::draw()
{
	blit(texture, posX, posY);
}

int PowerUp::getPosX()
{
	return posX;
}

int PowerUp::getPosY()
{
	return posY;
}

int PowerUp::getPowerUpWidth()
{
	return powerUpWidth;
}

int PowerUp::getPowerUpHeight()
{
	return powerUpHeight;
}
