#include "Bullet.h"

Bullet::Bullet(float postionX, float positionY, float directionX, float directionY, float speed, Side side)
{
	this->posX = postionX;
	this->posY = positionY;
	this->directionX = directionX;
	this->directionY = directionY;
	this->bulletSpeed = speed;
	this->side = side;
}

Bullet::~Bullet()
{
}

void Bullet::start()
{
	bulletWidth = 0;
	bulletHeight = 0;

	if (side == Side::PLAYER_SIDE)
	{
		texture = loadTexture("gfx/playerBullet.png");
	}
	else
	{
		texture = loadTexture("gfx/alienBullet.png");
	}

	SDL_QueryTexture(texture, NULL, NULL, &bulletWidth, &bulletHeight);
}

void Bullet::update()
{
	if (side == Side::PLAYER_SIDE)
	{
		posX += directionY * bulletSpeed;
		posY -= directionX * bulletSpeed;
	}
	else
	{
		posX += directionX * bulletSpeed;
		posY += directionY * bulletSpeed;
	}
}

void Bullet::draw()
{
	blit(texture, posX, posY);
}

int Bullet::getPosX()
{
	return posX;
}

int Bullet::getPosY()
{
	return posY;
}

int Bullet::getBulletWidth()
{
	return bulletWidth;
}

int Bullet::getBulletHeight()
{
	return bulletHeight;
}

Side Bullet::getSide()
{
	return side;
}
