#pragma once
#include "GameObject.h"
#include "common.h" 
#include "draw.h"
#include "SoundManager.h"
#include "Bullet.h"
#include <vector>
#include "util.h"
#include "Player.h"

class Enemy :
    public GameObject
{
public:
    Enemy();
    ~Enemy();
    void start();
    void update();
    void draw();
    void setPlayerTarget(Player* player);
    void setPosition(int x, int y);

    int getPosX();
    int getPosY();

    int getWidth();
    int getHeight();

private:
    SDL_Texture* texture; 
    Mix_Chunk* sound;
    Player* playerTarget;
    int posX;
    int posY;
    float directionX;
    float directionY;
    int enemyWidth;
    int enemyHeight;
    int enemySpeed;
    float reloadTime;
    float currentReloadTime;
    float directionChangeTime;
    float currentDirectionChangeTime;
    std::vector<Bullet*> bullets;
};

