#include "GameScene.h"

GameScene::GameScene()
{
	// Register and add game objects on constructor
	player = new Player();
	this->addGameObject(player);
	//do ^^this for any game object in the scene

	points = 0;
}

GameScene::~GameScene()
{
	delete player;
}

void GameScene::start()
{
	Scene::start();
	// Initialize any scene logic here

	initFonts();

	currentSpwanTimer = 300;
	spawnTime = 300; //spawn time of 5 seconds

	for (int i = 0; i < 3; i++)
	{
		spawn();
	}
}

void GameScene::draw()
{
	Scene::draw();

	drawText(110, 20, 255, 255, 255, TEXT_CENTER, "POINTS: %03d", points);

	if (player->getIsAlive() == false)
	{
		drawText(SCREEN_WIDTH / 2, 600, 255, 255, 255, TEXT_CENTER, "GAME OVER");
	}
}

void GameScene::update()
{
	Scene::update();

	spawnLogic();
	collisionLogic();
}

void GameScene::spawnLogic()
{
	if (currentSpwanTimer > 0)
		currentSpwanTimer--;

	if (currentSpwanTimer <= 0)
	{
		for (int i = 0; i < 3; i++)
		{
			spawn();
		}
		currentSpwanTimer = spawnTime;
	}
}

void GameScene::collisionLogic()
{
	for (int i = 0; i < objects.size(); i++)
	{
		// cast to bullet 
		Bullet* bullet = dynamic_cast<Bullet*>(objects[i]);

		// checks if objects[i] was a bullet
		if (bullet != NULL)
		{
			// if the bullet was from the enemy
			if (bullet->getSide() == Side::ENEMY_SIDE)
			{
				int collisison = checkCollision(
					player->getPosX(), player->getPosY(), player->getWidth(), player->getHeight(),
					bullet->getPosX(), bullet->getPosY(), bullet->getBulletWidth(), bullet->getBulletHeight()
				);

				if (collisison == 1)
				{
					std::cout << "player was hit!" << std::endl;
					player->dedEfx();
					break;
				}
			}
			else if (bullet->getSide() == Side::PLAYER_SIDE) // if bullet from player
			{
				for (int i = 0; i < spwanedEnemies.size(); i++)
				{
					Enemy* currentEnemy = spwanedEnemies[i];

					int collisison = checkCollision(
						currentEnemy->getPosX(), currentEnemy->getPosY(), currentEnemy->getWidth(), currentEnemy->getHeight(),
						bullet->getPosX(), bullet->getPosY(), bullet->getBulletWidth(), bullet->getBulletHeight()
					);

					if (collisison == 1)
					{
						std::cout << "enemy was hit!" << std::endl;
						despawnEnemy(currentEnemy);
						points++;
						break;
					}
				}
			}
		}
	}
}

void GameScene::spawn()
{
	Enemy* enemy = new Enemy();
	this->addGameObject(enemy);
	enemy->setPlayerTarget(player);

	enemy->setPosition(1200, 300 + (rand() % 300));
	spwanedEnemies.push_back(enemy);
}

void GameScene::despawnEnemy(Enemy* enemy)
{
	int index = -1;
	for (int i = 0; i < spwanedEnemies.size(); i++)
	{
		if (enemy == spwanedEnemies[i])
		{
			index = i;
			break;
		}
	}

	if (index != -1)
	{
		spwanedEnemies.erase(spwanedEnemies.begin() + index);
		delete enemy;
	}
}
