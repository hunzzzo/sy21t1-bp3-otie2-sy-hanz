#pragma once
#include "Scene.h"
#include "GameObject.h"
#include "player.h"
#include "Enemy.h"
#include <vector>
#include "text.h"

class GameScene : public Scene
{
public:
	GameScene();
	~GameScene();
	void start();
	void draw();
	void update();
private:
	Player* player;

	//enemy spawn logic
	float spawnTime;
	float currentSpwanTimer;
	std::vector<Enemy*> spwanedEnemies;

	void spawnLogic();
	void collisionLogic();

	void spawn();
	void despawnEnemy(Enemy* enemy);

	int points;
};

