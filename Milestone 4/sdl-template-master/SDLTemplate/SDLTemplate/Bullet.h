#pragma once
#include "GameObject.h"
#include "common.h" 
#include "draw.h"

enum class Side
{
    PLAYER_SIDE,
    ENEMY_SIDE
};

class Bullet :
    public GameObject
{
public:
    Bullet(float postionX, float positionY, float directionX, float directionY, float speed, Side side);
    void start();
    void update();
    void draw();

    int getPosX();
    int getPosY();

    int getBulletWidth();
    int getBulletHeight();

    Side getSide();

private:
    SDL_Texture* texture;
    Side side;
    int posX;
    int posY;
    int bulletWidth;
    int bulletHeight;
    int bulletSpeed;
    float directionX;
    float directionY;
};

