#pragma once
#include "GameObject.h"
#include "common.h" 
#include "draw.h" //include this when drawing any sprite (player, enemies, background, etc. )

class Player :
    public GameObject
{
public:
    void start();
    void update();
    void draw();

private:
    SDL_Texture* texture; //holds texture
    int posX; //tracks player position
    int posY;
    int playerWidth; //for collision
    int playerHeight; 
    int playerSpeed;
};

