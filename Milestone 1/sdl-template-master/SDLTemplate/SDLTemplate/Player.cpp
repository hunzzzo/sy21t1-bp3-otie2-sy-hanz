#include "Player.h"

void Player::start()
{
	//load the texture, call loadTexture function(found at draw.cpp), input directory to the file
	//SDL currently only supports jpeg, png, bitmap format
	texture = loadTexture("gfx/player.png");

	//initialize values
	posX = 100;
	posY = 100;
	playerWidth = 0;
	playerHeight = 0;
	playerSpeed = 2;

	//query texture to set width and height
	SDL_QueryTexture(texture, NULL, NULL, &playerWidth, &playerHeight);
}

void Player::update()
{
	//player controls are done under update

	//if "W" is pressed on the keyboard
	if (app.keyboard[SDL_SCANCODE_W]) //move up
	{
		posY -= playerSpeed;
	}

	if (app.keyboard[SDL_SCANCODE_S]) //move down
	{
		posY += playerSpeed;
	}

	if (app.keyboard[SDL_SCANCODE_A]) //move left
	{
		posX -= playerSpeed;
	}

	if (app.keyboard[SDL_SCANCODE_D]) //move right
	{
		posX += playerSpeed;
	}
}

void Player::draw()
{
	//call blit function to draw the texture
	//pass in texture, position X and Y
	blit(texture, posX, posY);

	//go to GameScene.h and #inlcude "player.h" there 
	//then create an instance of the player or any object 
}
