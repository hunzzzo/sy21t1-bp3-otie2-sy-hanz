#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"

class SnekFood :
    public GameObject
{
public:
    SnekFood();
    ~SnekFood();
    void start();
    void draw();
    void update();

    void setPosition(int x, int y);

    int getPosX();
    int getPosY();

    int getWidth();
    int getHeight();


private:
    SDL_Texture* texture;
    int posX;
    int posY;

    int width;
    int height;
};

