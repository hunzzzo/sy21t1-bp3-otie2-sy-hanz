#include "GameScene.h"

GameScene::GameScene()
{
	// Register and add game objects on constructor
}

GameScene::~GameScene()
{

}

void GameScene::start()
{
	Scene::start();
	// Initialize any scene logic here

	initFonts();
	gridMap = new GridMap();
	this->addGameObject(gridMap);

	snek = new Snek();
	this->addGameObject(snek);

	timer = 100;
	currenttimer = 100;

	spawnFood();
}

void GameScene::draw()
{
	Scene::draw();

	if (snek->getIsAlive() == false)
	{
		drawText(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 255, 255, 255, TEXT_CENTER, "GAME OVER");
	}
}

void GameScene::update()
{
	Scene::update();

	snekEatFood();
	snek->snekDeath();
}

void GameScene::spawnFood()
{
	std::cout << "food spawned" << std::endl;
	snekFood = new SnekFood();
	this->addGameObject(snekFood);
}

void GameScene::snekEatFood()
{
	int collision = checkCollision(snek->getHeadPosX(), snek->getHeadPosY(),
								   snek->getWidth(), snek->getHeight(),
								   snekFood->getPosX(), snekFood->getPosY(),
								   snekFood->getWidth(), snekFood->getHeight());
	if (collision == 1)
	{
		std::cout << "food was eaten" << std::endl;
		delete snekFood;
		spawnFood();
		snek->addSnekLength();
	}
}
