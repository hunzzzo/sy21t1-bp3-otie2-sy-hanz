#pragma once
#include "Scene.h"
#include "GameObject.h"
#include "GridMap.h"
#include "Snek.h"
#include "SnekFood.h"
#include "util.h"
#include "text.h"

class GameScene : public Scene
{
public:
	GameScene();
	~GameScene();
	void start();
	void draw();
	void update();

private:
	GridMap* gridMap;
	Snek* snek;
	SnekFood* snekFood;

	void spawnFood();

	void snekEatFood();

	int timer;
	int currenttimer;

};

