#pragma once
#include "GameObject.h"
#include "draw.h"
#include "common.h"
#include <vector>
#include "SnekFood.h"
#include "util.h"
#include "text.h"

class Snek :
    public GameObject
{
public:
    Snek();
    ~Snek();
    void start();
    void update();
    void draw();

    bool getIsAlive();

    void checkEdges();

    int getHeadNode();
    int getHeadPosX();
    int getHeadPosY();

    int getWidth();
    int getHeight();

    void addSnekLength();
    void snekDeath();

private:
    SDL_Texture* texture;
    std::vector<int> positionXs;
    std::vector<int> positionYs;
    int width;
    int height;
    int directionX;
    int directionY;
    float timer;
    float currentTimer;
    bool isAlive;
};

