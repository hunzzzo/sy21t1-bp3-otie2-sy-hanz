#include "GridMap.h"

GridMap::GridMap()
{
}

GridMap::~GridMap()
{
}

void GridMap::start()
{
	texture = loadTexture("gfx/Background.png");
	gridWidth = 35;
	gridHeight = 20;
	gridSize = 32;
}

void GridMap::draw()
{
	for (int i = 0; i < gridWidth; i++)
	{
		for (int j = 0; j < gridHeight; j++)
		{
			blit(texture, i * gridSize, j * gridSize);
		}
	}
}

void GridMap::update()
{
}

int GridMap::getGridWidth()
{
	return gridWidth;
}

int GridMap::getGridHeight()
{
	return gridHeight;
}

int GridMap::getGridSize()
{
	return gridSize;
}
