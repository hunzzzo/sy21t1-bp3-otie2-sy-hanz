#include "SnekFood.h"

SnekFood::SnekFood()
{
}

SnekFood::~SnekFood()
{
}

void SnekFood::start()
{
	texture = loadTexture("gfx/SnekFood.png");
	posX = rand() % 35 * 32;
	posY = rand() % 20 * 32;
	width = 32;
	height = 32;
}

void SnekFood::draw()
{
	blit(texture, posX, posY);
}

void SnekFood::update()
{
}

void SnekFood::setPosition(int x, int y)
{
	x = posX;
	y = posY;
}

int SnekFood::getPosX()
{
	return posX;
}

int SnekFood::getPosY()
{
	return posY;
}

int SnekFood::getWidth()
{
	return width;
}

int SnekFood::getHeight()
{
	return height;
}
