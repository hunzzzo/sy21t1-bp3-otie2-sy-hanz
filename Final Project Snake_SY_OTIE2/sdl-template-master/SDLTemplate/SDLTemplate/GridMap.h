#pragma once
#include "GameObject.h"
#include "common.h"
#include "draw.h"
#include <vector>

class GridMap :
    public GameObject
{
public:
    GridMap();
    ~GridMap();
    void start();
    void draw();
    void update();

    int getGridWidth();
    int getGridHeight();
    int getGridSize();

private:
    SDL_Texture* texture;
    int gridWidth;
    int gridHeight;
    int gridSize;

};

