#include "Snek.h"

Snek::Snek()
{
}

Snek::~Snek()
{
}

void Snek::start()
{
	texture = loadTexture("gfx/Snek.png");
	width = 32;
	height = 32;
	
	directionX = 1;
	directionY = 0;
	timer = 20;
	currentTimer = 20;

	isAlive = true;

	positionXs.push_back(8 * width);
	positionYs.push_back(8 * height);

	positionXs.push_back(9 * width);
	positionYs.push_back(8 * height);

	positionXs.push_back(10 * width);
	positionYs.push_back(8 * height);
}

void Snek::update()
{
	if (!isAlive) return;

	if (currentTimer > 0)
	{
		currentTimer--;
	}

	if (currentTimer <= 0)
	{
		checkEdges();

		for (int i = 0; i < positionXs.size() - 1; i++)
		{
			int next = i + 1;
			positionXs[i] = positionXs[next];
			positionYs[i] = positionYs[next];
		}

		positionXs[getHeadNode()] += directionX * width;
		positionYs[getHeadNode()] += directionY * height;
		
		currentTimer = timer;
	}
		
	if (app.keyboard[SDL_SCANCODE_W]) //move up
	{
		if (directionY != 1)
		{
			directionX = 0;
			directionY = -1;
		}
	}
	else if (app.keyboard[SDL_SCANCODE_S]) //move down
	{
		if (directionY != -1)
		{
			directionX = 0;
			directionY = 1;
		}
	}
	else if (app.keyboard[SDL_SCANCODE_A]) //move left
	{
		if (directionX != 1)
		{
			directionX = -1;
			directionY = 0;
		}
	}
	else if (app.keyboard[SDL_SCANCODE_D]) //move right
	{
		if (directionX != -1)
		{
			directionX = 1;
			directionY = 0;
		}
	}
}

void Snek::draw()
{
	if (!isAlive) return;

	for (int i = 0; i < positionXs.size(); i++)
	{
		blit(texture, positionXs[i], positionYs[i]);
	}
}

bool Snek::getIsAlive()
{
	return isAlive;
}

void Snek::checkEdges()
{
	if (positionXs[getHeadNode()] < 0) //if go beyond left
	{
		positionXs[getHeadNode()] = SCREEN_WIDTH;
		directionX = -1;
	}
	
	if (positionXs[getHeadNode()] > SCREEN_WIDTH) //if go beyond right
	{
		positionXs[getHeadNode()] = 0;
		directionX = 1;
	}

	if (positionYs[getHeadNode()] < 0) //if go beyond top
	{
		positionYs[getHeadNode()] = SCREEN_HEIGHT;
		directionY = -1;
	}

	if (positionYs[getHeadNode()] > SCREEN_HEIGHT) //if go beyond bottom
	{
		positionYs[getHeadNode()] = 0;
		directionY = 1;
	}
}

int Snek::getHeadNode()
{
	return positionXs.size() - 1;
}

int Snek::getHeadPosX()
{
	return positionXs[positionXs.size() - 1];
}

int Snek::getHeadPosY()
{
	return positionYs[positionYs.size() - 1];
}

int Snek::getWidth()
{
	return width;
}

int Snek::getHeight()
{
	return height;
}

void Snek::addSnekLength()
{
	//at first the new part of the snek was spawned on the head
	// 	   and then it just finds its way to the end of the body
	//upon adding death condition, the code is broken and i dunno how to fix it
	positionXs.push_back(getHeadNode());
	positionYs.push_back(getHeadNode());
	std::cout << positionXs[0] << std::endl;
	std::cout << getHeadPosX() << std::endl;
	std::cout << getHeadPosY() << std::endl;
	timer--;
	currentTimer--;
	std::cout << timer << std::endl;
}

void Snek::snekDeath()
{
	for (int i = 0; i < positionXs.size() - 1; i++)
	{
		int collision = checkCollision(getHeadPosX(), getHeadPosY(), width, height,
									   positionXs[i], positionYs[i], width, height);
		
		if (collision == 1)
		{
			std::cout << "player is dead" << std::endl;
			isAlive = false;
			break;
		}
	}
}


