#pragma once
#include "GameObject.h"
#include "common.h" 
#include "draw.h"

class Bullet :
    public GameObject
{
public:
    Bullet(float postionX, float positionY, float directionX, float directionY, float speed);
    void start();
    void update();
    void draw();

    float getPosX();
    float getPosY();

    float getBulletWidth();
    float getBulletHeight();

private:
    SDL_Texture* texture;
    int posX;
    int posY;
    int bulletWidth;
    int bulletHeight;
    int bulletSpeed;
    float directionX;
    float directionY;
};

