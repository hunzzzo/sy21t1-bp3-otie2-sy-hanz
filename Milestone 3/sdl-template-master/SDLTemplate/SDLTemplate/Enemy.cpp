#include "Enemy.h"
#include "Scene.h"

Enemy::Enemy()
{
}

Enemy::~Enemy()
{
}

void Enemy::start()
{
	texture = loadTexture("gfx/enemy.png");

	directionX = -1;
	directionY = 1;
	enemyWidth = 0;
	enemyHeight = 0;
	enemySpeed = 2;

	reloadTime = 60; //reload time of 60 frames, 1 second 
	currentReloadTime = 0;

	directionChangeTime = (rand() % 300) + 180; // randomize value from 0 to 300, direction change time of 3 - 8 seconds
	currentDirectionChangeTime = 0;

	SDL_QueryTexture(texture, NULL, NULL, &enemyWidth, &enemyHeight);

	sound = SoundManager::loadSound("sound/334227__jradcoolness__laser.ogg");
}

void Enemy::update()
{
	posX += directionX * enemySpeed;
	posY += directionY * enemySpeed;

	if (currentDirectionChangeTime > 0)
		currentDirectionChangeTime--;

	if (currentDirectionChangeTime == 0)
	{
		directionY = -directionY;
		currentDirectionChangeTime = directionChangeTime;
	}

	if (currentReloadTime > 0)
		currentReloadTime--;

	if (currentReloadTime == 0)
	{
		float dx = -1;
		float dy = 0;

		calcSlope(playerTarget->getPosX(), playerTarget->getPosY(), posX, posY, &dx, &dy);

		SoundManager::playSound(sound);
		Bullet* bullet = new Bullet(posX + enemyWidth, posY - 2 + enemyHeight / 2, dx, dy, 10);
		bullets.push_back(bullet);
		getScene()->addGameObject(bullet);

		//starts realod time 
		currentReloadTime = reloadTime;
	}

	for (int i = 0; i < bullets.size(); i++)
	{
		if (bullets[i]->getPosX() < 0)
		{	
			Bullet* bulletToErase = bullets[i];
			bullets.erase(bullets.begin() + i);
			delete bulletToErase;

			break;
		}
	}
}

void Enemy::draw()
{
	blit(texture, posX, posY);
}

void Enemy::setPlayerTarget(Player* player)
{
	playerTarget = player;
}

void Enemy::setPosition(int x, int y)
{
	this->posX = x;
	this->posY = y;
}
