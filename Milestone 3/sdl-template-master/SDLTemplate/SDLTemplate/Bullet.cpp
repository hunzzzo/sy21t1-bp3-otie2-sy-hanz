#include "Bullet.h"

Bullet::Bullet(float postionX, float positionY, float directionX, float directionY, float speed)
{
	this->posX = postionX;
	this->posY = positionY;
	this->directionX = directionX;
	this->directionY = directionY;
	this->bulletSpeed = speed;
}

void Bullet::start()
{
	bulletWidth = 0;
	bulletHeight = 0;
	texture = loadTexture("gfx/playerBullet.png");

	SDL_QueryTexture(texture, NULL, NULL, &bulletWidth, &bulletHeight);
}

void Bullet::update()
{
	posX += directionX * bulletSpeed;
	posY += directionY * bulletSpeed;

}

void Bullet::draw()
{
	blit(texture, posX, posY);
}

float Bullet::getPosX()
{
	return posX;
}

float Bullet::getPosY()
{
	return posY;
}

float Bullet::getBulletWidth()
{
	return bulletWidth;
}

float Bullet::getBulletHeight()
{
	return bulletHeight;
}
