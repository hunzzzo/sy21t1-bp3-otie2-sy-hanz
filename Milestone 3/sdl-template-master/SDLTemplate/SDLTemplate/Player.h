#pragma once
#include "GameObject.h"
#include "common.h" 
#include "draw.h" //include this when drawing any sprite (player, enemies, background, etc. )
#include "SoundManager.h"
#include "Bullet.h"
#include <vector>

class Player :
    public GameObject
{
public:
    ~Player();
    void start();
    void update();
    void draw();

    int getPosX();
    int getPosY();

private:
    SDL_Texture* texture; //holds texture
    Mix_Chunk* sound;
    int posX; //tracks player position
    int posY;
    int playerWidth; //for collision
    int playerHeight; 
    int playerSpeed;
    float reloadTime;
    float currentReloadTime;
    std::vector<Bullet*> bullets;
};

